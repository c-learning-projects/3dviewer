#ifndef SRC_S21_MATRIX_H_
#define SRC_S21_MATRIX_H_

#include <stdlib.h>
#include <stdio.h>

#ifndef SUCCESS
#define SUCCESS 1
#endif
#ifndef FAILURE
#define FAILURE 0
#endif

#ifndef SUB
#define SUB 0
#endif
#ifndef SUM
#define SUM 1
#endif
#ifndef MULT
#define MULT 2
#endif
#ifndef EPS
#define EPS 0.0000000999
#endif

#define J_OP 0
#define MATRIX_MULT_OP 1
#define CORRECT_ONLY -1
#define SQUARE 2

#ifndef S21_NAN
#define S21_NAN (0.0 / 0.0)
#endif

#ifndef S21_INF
#define S21_INF (1.0 / 0.0)
#endif

typedef enum {
    CORRECT_MATRIX = 0,
    INCORRECT_MATRIX = 1,
    IDENTITY_MATRIX = 2,
    ZERO_MATRIX = 3
} matrix_type_t;

typedef struct matrix_struct {
    double** matrix;
    int rows;
    int columns;
    matrix_type_t matrix_type;
} matrix_t;

matrix_t s21_create_matrix(int rows, int columns);
void s21_remove_matrix(matrix_t *A);
int s21_eq_matrix(matrix_t *A, matrix_t *B);
matrix_t s21_sum_matrix(matrix_t *A, matrix_t *B);
matrix_t s21_sub_matrix(matrix_t *A, matrix_t *B);
matrix_t s21_mult_number(matrix_t *A, double number);
matrix_t s21_mult_matrix(matrix_t *A, matrix_t *B);
matrix_t s21_transpose(matrix_t *A);
matrix_t s21_calc_complements(matrix_t *A);
double s21_determinant(matrix_t *A);
matrix_t s21_inverse_matrix(matrix_t *A);

int s21_equal(double aij, double bij);
int s21_check_for_volid(matrix_t *A, matrix_t *B, int check_option);
matrix_t s21_sum_sub_mult_matrix(matrix_t *A, matrix_t *B, int operator, double num);
matrix_t s21_minor_matrix(matrix_t *A, int n, int m);
matrix_t s21_matrix_of_minors(matrix_t *A);
int s21_1_or_minus_1(int n);
long double s21_fabs(double x);
int s21_its_identity_matrix(matrix_t *A);
int s21_its_zero_matrix(matrix_t *A);
int s21_get_matrix_type(matrix_t *A);
int s21_its_incorrect_matrix(matrix_t *A);
int s21_isinf(double s21_x);
int s21_isnan(double s21_x);

#endif  // SRC_S21_MATRIX_H_
