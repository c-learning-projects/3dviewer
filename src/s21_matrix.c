#include "s21_matrix.h"

matrix_t s21_create_matrix(int rows, int columns) {
    matrix_t new;
    if (rows < 1 || columns < 1) {
        new.matrix = NULL;
        new.matrix_type = INCORRECT_MATRIX;
    } else {
        new.matrix = (double**)calloc(rows, sizeof(double*));
        for (int i = 0; i < rows; i++) {
            new.matrix[i] = (double*)calloc(columns, sizeof(double));
        }
        new.matrix_type = ZERO_MATRIX;
    }
    new.columns = columns;
    new.rows = rows;
    return new;
}

void s21_remove_matrix(matrix_t *A) {
    for (int i = 0; i < (*A).rows; i++) free((*A).matrix[i]);
    if ((*A).rows > 0) free((*A).matrix);
    (*A).columns = 0;
    (*A).rows = 0;
    (*A).matrix_type = INCORRECT_MATRIX;
}

int s21_eq_matrix(matrix_t *A, matrix_t *B) {
    int res = SUCCESS;
    if (s21_check_for_volid(A, B, J_OP)) {
        for (int i = 0; i < (*A).rows && res == SUCCESS; i++) {
            for (int j = 0; j < (*A).columns; j++) {
                if (s21_fabs((*A).matrix[i][j] - (*B).matrix[i][j]) > EPS) res = FAILURE;
            }
        }
    } else {
        res = FAILURE;
    }
    return res;
}

matrix_t s21_sum_matrix(matrix_t *A, matrix_t *B) {
    return s21_sum_sub_mult_matrix(A, B, SUM, 0);
}

matrix_t s21_sub_matrix(matrix_t *A, matrix_t *B) {
    return s21_sum_sub_mult_matrix(A, B, SUB, 0);
}

matrix_t s21_mult_number(matrix_t *A, double number) {
    return s21_sum_sub_mult_matrix(A, A, MULT, number);
}

matrix_t s21_mult_matrix(matrix_t *A, matrix_t *B) {
    matrix_t C = s21_create_matrix(0, 0);
    if (s21_check_for_volid(A, B, MATRIX_MULT_OP)) {
        C = s21_create_matrix((*A).rows, (*B).columns);
        for (int i = 0; i < (*A).rows; i++) {
            for (int j = 0; j < (*B).columns; j++) {
                for (int k = 0; k < (*B).rows; k++) {
                    C.matrix[i][j] += (*A).matrix[i][k] * (*B).matrix[k][j];
                }
            }
        }
        C.matrix_type = s21_get_matrix_type(&C);
    }
    return C;
}

matrix_t s21_transpose(matrix_t *A) {
    matrix_t C = s21_create_matrix(0, 0);
    if (s21_check_for_volid(A, A, CORRECT_ONLY)) {
        C = s21_create_matrix((*A).columns, (*A).rows);
        for (int i = 0; i < (*A).rows; i++) {
            for (int j = 0; j < (*A).columns; j++) {
                C.matrix[j][i] = (*A).matrix[i][j];
            }
        }
        C.matrix_type = s21_get_matrix_type(&C);
    }
    return C;
}

matrix_t s21_calc_complements(matrix_t *A) {
    matrix_t C;
    if (s21_check_for_volid(A, A, SQUARE) && (*A).rows != 1) {
        C = s21_create_matrix((*A).rows, (*A).columns);
                for (int i = 0; i < (*A).rows; i++) {
                    for (int j = 0; j < (*A).columns; j++) {
                        matrix_t minor = s21_minor_matrix(A, i, j);
                        C.matrix[i][j] = s21_1_or_minus_1(i + j) * s21_determinant(&minor);
                        s21_remove_matrix(&minor);
                    }
                }
                C.matrix_type = s21_get_matrix_type(&C);
        } else {
            C = s21_create_matrix(0, 0);
        }
    return C;
}

double s21_determinant(matrix_t *A) {
    double res = 0;
    if (!s21_check_for_volid(A, A, SQUARE)) {
        res = S21_NAN;
    } else if ((*A).rows == 1) {
        res = (*A).matrix[0][0];
    } else if ((*A).rows == 2) {
            res = (*A).matrix[0][0] * (*A).matrix[1][1] -
                    (*A).matrix[1][0] * (*A).matrix[0][1];
    } else {
        for (int i = 0; i < (*A).columns; i++) {
            matrix_t minor_matrix = s21_minor_matrix(A, 0, i);
            res += s21_1_or_minus_1(i) * (*A).matrix[0][i] * s21_determinant(&minor_matrix);
            s21_remove_matrix(&minor_matrix);
        }
    }
    return res;
}

matrix_t s21_inverse_matrix(matrix_t *A) {
    matrix_t C;
    double d = s21_determinant(A);
    if (s21_check_for_volid(A, A, SQUARE) && d != 0) {
        if ((*A).rows == 1) {
            C = s21_create_matrix((*A).rows, (*A).columns);
            C.matrix[0][0] = 1 / d;
        } else {
            matrix_t cm = s21_calc_complements(A);
            matrix_t tm = s21_transpose(&cm);
            C = s21_mult_number(&tm, 1 / d);
            s21_remove_matrix(&cm);
            s21_remove_matrix(&tm);
        }
        C.matrix_type = s21_get_matrix_type(&C);
    } else {
        C = s21_create_matrix(0, 0);
    }
    return C;
}

int s21_get_matrix_type(matrix_t *A) {
    int zero = 1, ident = 1, correct = 1, res = 0;
    if (!s21_check_for_volid(A, A, SQUARE)) {
        ident = 0;
    }
    for (int i = 0; i < (*A).rows; i++) {
        for (int j = 0; j < (*A).columns; j++) {
            if (s21_fabs((*A).matrix[i][j]) > EPS) zero = 0;
            if (s21_isnan((*A).matrix[i][j]) || s21_isinf((*A).matrix[i][j])) correct = 0;
            if (i == j) {
                if ((s21_fabs((*A).matrix[i][j]) - 1.0) > EPS) {
                    ident = 0;
                }
            } else {
                if (s21_fabs((*A).matrix[i][j]) > EPS) {
                    ident = 0;
                }
            }
        }
    }
    if (!correct) res = INCORRECT_MATRIX;
    else if (zero) res = ZERO_MATRIX;
    else if (ident) res = IDENTITY_MATRIX;
    return res;
}

long double s21_fabs(double x) {
    long double res = x;
    if (x < 0.0) res = -res;
    return res;
}

int s21_1_or_minus_1(int n) {
    int res = -1;
    if (n % 2 == 0) res = 1;
    return res;
}

matrix_t s21_minor_matrix(matrix_t *A, int n, int m) {
    matrix_t C = s21_create_matrix((*A).rows - 1, (*A).columns - 1);
    int i_now = 0, j_now = 0;
    for (int i = 0; i < (*A).rows; i++) {
        if (i != n) {
            for (int j = 0; j < (*A).columns; j++) {
                if (i != n && j != m) {
                    C.matrix[i_now][j_now] = (*A).matrix[i][j];
                    j_now++;
                }
            }
            j_now = 0;
            i_now++;
        }
    }
    return C;
}

matrix_t s21_sum_sub_mult_matrix(matrix_t *A, matrix_t *B, int operator, double num) {
    matrix_t C = s21_create_matrix(0, 0);
    if (s21_check_for_volid(A, B, J_OP)) {
        C = s21_create_matrix((*A).rows, (*A).columns);
        for (int i = 0; i < (*A).rows; i++) {
            for (int j = 0; j < (*A).columns; j++) {
                if (operator == SUB) {
                    C.matrix[i][j] = (*A).matrix[i][j] - (*B).matrix[i][j];
                } else if (operator == SUM) {
                    C.matrix[i][j] = (*A).matrix[i][j] + (*B).matrix[i][j];
                } else if (operator == MULT) {
                    C.matrix[i][j] = (*A).matrix[i][j] * num;
                }
            }
        }
        C.matrix_type = s21_get_matrix_type(&C);
    }
    return C;
}

int s21_check_for_volid(matrix_t *A, matrix_t *B, int check_option) {
    int correct = (((*A).matrix_type != INCORRECT_MATRIX) && ((*B).matrix_type != INCORRECT_MATRIX));
    int res = correct;
    if (check_option == J_OP) {
        res = (correct && ((*A).columns == (*B).columns) && ((*A).rows == (*B).rows));
    } else if (check_option == MATRIX_MULT_OP) {
        res = (correct && ((*A).columns == (*B).rows));
    } else if (check_option == SQUARE) {
        res = (correct && ((*A).rows == (*A).columns));
    }
    return res;
}

int s21_isinf(double s21_x) {
    return s21_fabs(s21_x) == S21_INF;
}

int s21_isnan(double s21_x) {
    return s21_x != s21_x;
}
