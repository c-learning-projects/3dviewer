#ifndef SRC_VECTORS_H_
#define SRC_VECTORS_H_
#include <math.h>
typedef struct {
    float x;
    float y;
    float z;
    float w;
} vector;

vector substruct(vector v1, vector v2);
vector crossProduct(vector a, vector b);
float  getLength(vector a);
vector normalize(vector a);
vector vector_create(float a, float b, float c);
#endif  // SRC_VECTORS_H_
