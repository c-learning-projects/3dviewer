#include <stdlib.h>
#include <stdio.h>
#include <gtk/gtk.h>
#include <cairo.h>
#include "sub.h"
#include "file_reader.h"
#include "calc.h"
#include <math.h>
#include <ImageMagick-7/MagickWand/MagickWand.h>

GtkBuilder *ui_builder;
coord *Mymass;
polygons *models;

float angel_x = 0, angel_y = 0, angel_z = 0, scale_step = 1,
move_x = 0, move_y = 0, move_z = 0;
int record = 0;

G_MODULE_EXPORT void cb_proection_type_changed_cb(GtkWidget *widget) {
    int cb_proection_type_index = gtk_combo_box_get_active(
                                    GTK_COMBO_BOX(GTK_WIDGET(
                                        gtk_builder_get_object(ui_builder, "cb_proection_type"))));
    GtkWidget* ent_k = GTK_WIDGET(gtk_builder_get_object(ui_builder, "ent_k"));
    if (!cb_proection_type_index) {
        gtk_widget_hide(ent_k);
    } else {
        gtk_widget_show(ent_k);
    }
}

void buf_clean() {
    int triangles = get_int_from_label("lable_triangles_num", &ui_builder);
    for (int i = 0; i < triangles; i++) {
        free(models[i].point);
    }
    if (Mymass != NULL) free(Mymass);
    if (models != NULL) free(models);
}

G_MODULE_EXPORT void onExit(GtkWidget *widget) {
    buf_clean();
    FILE* file_config = fopen("config.conf", "wb");
    int cb_vertices_index = gtk_combo_box_get_active(
                                GTK_COMBO_BOX(GTK_WIDGET(
                                    gtk_builder_get_object(ui_builder, "cb_vertices"))));
    int cb_line_index = gtk_combo_box_get_active(
                            GTK_COMBO_BOX(GTK_WIDGET(
                                gtk_builder_get_object(ui_builder, "cb_lines"))));
    int cb_proection_type_index = gtk_combo_box_get_active(
                            GTK_COMBO_BOX(GTK_WIDGET(
                                gtk_builder_get_object(ui_builder, "cb_proection_type"))));
    int vertices_size = get_double_from_entry("etr_vertices_size", &ui_builder);
    int lines_size = get_double_from_entry("ent_lines_size", &ui_builder);
    int ent_k = get_double_from_entry("ent_k", &ui_builder);
    fprintf(file_config, "%d %d %d %d %d %s %s %s %s %s %d", cb_vertices_index, cb_line_index,
            cb_proection_type_index, vertices_size, lines_size,
            get_text_from_widget("ent_line_rgb", &ui_builder),
            get_text_from_widget("ent_lines_size", &ui_builder),
            get_text_from_widget("ent_vertices_rgb", &ui_builder),
            get_text_from_widget("etr_vertices_size", &ui_builder),
            get_text_from_widget("ent_background_rgb", &ui_builder),
            ent_k);
    fclose(file_config);
    gtk_main_quit();
}

void convert_x_y_to_pix(float height, float width, float* x, float* y,
                        float* z, int cb_proection_type_index) {
    width /= 2.0; height /= 2.0; float k = get_double_from_entry("ent_k", &ui_builder);
    if (cb_proection_type_index) {*x = (k * *x) / (*z + k); *y = (k * *y) / (*z + k);}
    *x = *x * width + width;
    *y = height - *y * height;
}

void new_v_draw(cairo_t* cr, int vertices, int n, float size, int height, int width) {
    if (n) {
    set_rgb_from_entry(cr, "ent_vertices_rgb", &ui_builder);
        for (int i = 0; i < vertices; i++) {
            if (n == 1) cairo_arc(cr, Mymass[i].x, Mymass[i].y, size, 0., M_PIf * 2.0);
            if (n == 2) cairo_rectangle(cr, Mymass[i].x - size / 2, Mymass[i].y - size / 2, size, size);
            cairo_fill(cr);
        }
        cairo_stroke(cr);
    }
}

void make_vertc(float x, float y, int n) {
    Mymass[n].x = x;
    Mymass[n].y = y;
}

G_MODULE_EXPORT void btn_record_clicked_cb(GtkWidget *widget) {
    GtkButton* btn = GTK_BUTTON(gtk_builder_get_object(ui_builder, "btn_record"));
    const char* text = gtk_button_get_label(btn);
    if (!strcmp(text, "Record")) {
        record = 1;
        gtk_button_set_label(btn, "Stop");
    } else {
        MagickWand * wand;
        wand = NewMagickWand();
        char temp[20];
        for (int i = 1; i < record; i++) {
            snprintf(temp, sizeof(temp), "record/%d.png", i);
            MagickReadImage(wand, temp);
        }
        MagickWriteImages(wand, "output.gif", MagickTrue);
        MagickWandTerminus();
        record = 0;
        gtk_button_set_label(btn, "Record");
    }
}

void make_screenshot(int screen) {
    if (screen || record) {
        GError *error = NULL;
        int height = get_widget_height("drawarea", &ui_builder),
        width = get_widget_width("drawarea", &ui_builder);
        GdkPixbuf* pixbuf = gdk_pixbuf_get_from_window(
                                gtk_widget_get_window(GTK_WIDGET(
                                    gtk_builder_get_object(ui_builder, "drawarea"))), 0, 0, width, height);
        int cb_screenshot_format_index = gtk_combo_box_get_active(
                                            GTK_COMBO_BOX(GTK_WIDGET(
                                                gtk_builder_get_object(
                                                    ui_builder, "cb_screenshot_format"))));
        if (screen) {
            if (!cb_screenshot_format_index)
                gdk_pixbuf_save(pixbuf, "creenshot.jpeg", "jpeg", &error, "quality", "100", NULL);
            else
                gdk_pixbuf_save(pixbuf, "creenshot.bmp", "bmp", &error, "quality", "100", NULL);
        }
        if (!screen && record) {
            char temp[20];
            snprintf(temp, sizeof(temp), "record/%d.png", record);
            gdk_pixbuf_save(pixbuf, temp, "png",
                                                        &error, NULL);
                                                        record++;
        if (record == 51) btn_record_clicked_cb(GTK_WIDGET(
                                                gtk_builder_get_object(ui_builder, "btn_record")));
        }
    }
}

G_MODULE_EXPORT void bt_sreenshot_clicked_cb(GtkWidget *widget) {
    make_screenshot(1);
}

void grani(matrix_t* A, cairo_t* cr, polygons models, float height, float width, int
            cb_proection_type_index, int need) {
    if (need) {
        float x = 0, y = 0, z = 0;
        for (int j = 0; j < 4; j++) {
            int print = 0, temp_x = 0, temp_y = 0;
            if (((j == 3) && (models.point[j].x != 0.0 || models.point[j].y != 0.0
                    || models.point[j].z != 0.0)) || (j != 3)) print = 1;
            if (j < 4 && print) {
                xyz_res(A, models.point[j].x,  models.point[j].y,  models.point[j].z, &x, &y, &z);
                convert_x_y_to_pix(height, width, &x, &y, &z, cb_proection_type_index);
            }
            if (print && j == 0) {
                cairo_move_to(cr, x, y); temp_x = x;
            }
            if (j == 4 && print) {
                cairo_line_to(cr, temp_x, temp_y);
            } else {
                cairo_line_to(cr, x, y);
            }
        }
        set_rgb_from_entry(cr, "ent_line_rgb", &ui_builder);
        cairo_stroke(cr);
        set_rgb_from_entry(cr, "ent_fill_rgb", &ui_builder);
    }
}

void draw() {
    GtkWidget* drawarea = GTK_WIDGET(gtk_builder_get_object(ui_builder, "drawarea"));
    GtkWidget* file_chooser = GTK_WIDGET(gtk_builder_get_object(ui_builder, "file_open_btm"));
    GdkWindow* window = gtk_widget_get_window(drawarea);
    cairo_region_t* cairoRegion = cairo_region_create();

    GdkDrawingContext* drawingContext = gdk_window_begin_draw_frame(window, cairoRegion);
    cairo_t* cr = gdk_drawing_context_get_cairo_context(drawingContext);
    int height = get_widget_height("drawarea", &ui_builder),
    width = get_widget_width("drawarea", &ui_builder);
    draw_area_clean(cr, &ui_builder);

    int triangles = get_int_from_label("lable_triangles_num", &ui_builder);
    int cb_vertices_index = gtk_combo_box_get_active(
                                GTK_COMBO_BOX(GTK_WIDGET(
                                    gtk_builder_get_object(ui_builder, "cb_vertices"))));
    int cb_line_index = gtk_combo_box_get_active(
                            GTK_COMBO_BOX(GTK_WIDGET(gtk_builder_get_object(ui_builder, "cb_lines"))));
    float vertices_size = get_double_from_entry("etr_vertices_size", &ui_builder);
    float lines_size = get_double_from_entry("ent_lines_size", &ui_builder);
    int cb_proection_type_index = gtk_combo_box_get_active(
                            GTK_COMBO_BOX(GTK_WIDGET(
                                gtk_builder_get_object(ui_builder, "cb_proection_type"))));
    float x = 0, y = 0, z = 0;
    int fill = gtk_toggle_button_get_active(
                        GTK_TOGGLE_BUTTON(gtk_builder_get_object(ui_builder, "check_box_fill")));

    const double dashed1[] = {5.0, 5.0, 5.0};
    if (cb_line_index == 1) cairo_set_dash(cr, dashed1, sizeof(dashed1) / sizeof(dashed1[0]), 1);
    else
        cairo_set_dash(cr, dashed1, 0, 0);
    cairo_set_line_width(cr, lines_size);
    cairo_set_line_join(cr, CAIRO_LINE_JOIN_ROUND);
    set_rgb_from_entry(cr, "ent_line_rgb", &ui_builder);
    int need_grany = need_grani(cr, "ent_line_rgb", "ent_fill_rgb", &ui_builder);

    matrix_t A = create_moves(move_x, move_y, move_z, angel_x, angel_y, angel_z, scale_step);
    int vertices = 0;
    Mymass = (coord*)realloc(Mymass, (triangles * 5 + 1) * sizeof(coord));
    for (int i = 0; i < triangles; i++) {
        for (int j = 0; j < 4; j++) {
            int print = 0, temp_x = 0, temp_y = 0;
            if (((j == 3) && (models[i].point[j].x != 0.0 || models[i].point[j].y != 0.0
                    || models[i].point[j].z != 0.0)) || (j != 3)) print = 1;
            if (j < 4 && print) {
                xyz_res(&A, models[i].point[j].x,  models[i].point[j].y,  models[i].point[j].z, &x, &y, &z);
                convert_x_y_to_pix(height, width, &x, &y, &z, cb_proection_type_index);
            }
            if (print && j == 0) {
                cairo_move_to(cr, x, y); temp_x = x;
            }
            if (j == 4 && print) {
                cairo_line_to(cr, temp_x, temp_y);
                vertices++;
                make_vertc(temp_x, temp_y, vertices);
            } else {
                cairo_line_to(cr, x, y);
                vertices++;
                make_vertc(x, y, vertices);
            }
        }
        if (fill) {
            cairo_close_path(cr);
            cairo_fill(cr);
            grani(&A, cr, models[i], height, width, cb_proection_type_index, need_grany);
        }
        cairo_stroke(cr);
    }
    new_v_draw(cr, vertices, cb_vertices_index, vertices_size, height, width);
    s21_remove_matrix(&A);
    gdk_window_end_draw_frame(window, drawingContext);
    cairo_region_destroy(cairoRegion);
}

void draw_with_timer() {
    clock_t start = clock();
    set_label_num("label_drawing_time", 0, &ui_builder);
    draw();
    clock_t end = clock();
    set_label_num("label_drawing_time", (end - start) / (CLOCKS_PER_SEC / 1000), &ui_builder);
}

G_MODULE_EXPORT void file_open_btm_file_set_cb(GtkWidget *widget) {
    buf_clean();
    GtkFileChooser* file_chooser = GTK_FILE_CHOOSER(
                                    GTK_WIDGET(
                                        gtk_builder_get_object(ui_builder, "file_open_btm")));
    set_label_text("label_name_of_model", gtk_file_chooser_get_filename(file_chooser), &ui_builder);
    int vertices = 0, triangles = 0;
    char name[150];
    Read_file_obj(gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(file_chooser)),
                                                    &Mymass, &models, &vertices, &triangles, name);
    set_label_num("label_vertices_num", vertices, &ui_builder);
    set_label_num("lable_triangles_num", triangles, &ui_builder);
    set_label_text("label_name_str", name, &ui_builder);
    angel_x = 0, angel_y = 0, angel_z = 0, scale_step = 1,
    move_x = 0, move_y = 0, move_z = 0;
}

G_MODULE_EXPORT void btn_draw_clicked_cb() {
    draw_with_timer();
}

G_MODULE_EXPORT void btn_scale_minus_clicked_cb() {
    scale_step = scale_step * (1 / (get_double_from_entry("scale_step", &ui_builder) / 100 + 1));
    draw_with_timer();
    make_screenshot(0);
}

G_MODULE_EXPORT void btn_scale_plus_clicked_cb() {
    scale_step = scale_step * (get_double_from_entry("scale_step", &ui_builder) / 100 + 1);
    draw_with_timer();
    make_screenshot(0);
}

G_MODULE_EXPORT void bt1_clicked() {
    angel_x = angel_x + get_double_from_entry("enty_turn_x", &ui_builder);
    angel_y = angel_y + get_double_from_entry("enty_turn_y", &ui_builder);
    angel_z = angel_z + get_double_from_entry("enty_turn_z", &ui_builder);
    draw_with_timer();
    make_screenshot(0);
}

G_MODULE_EXPORT void btn2_clicked() {
    move_x = move_x + get_double_from_entry("etr_move_x", &ui_builder);
    move_y = move_y + get_double_from_entry("etr_move_y", &ui_builder);
    move_z = move_z + get_double_from_entry("etr_move_z", &ui_builder);
    draw_with_timer();
    make_screenshot(0);
}

G_MODULE_EXPORT void drawarea_draw_cb() {
    draw_with_timer();
}

int main(int argc, char *argv[]) {
    gtk_init(&argc, &argv);
    GError *err = NULL;
    ui_builder = gtk_builder_new();
    if (!gtk_builder_add_from_file(ui_builder, "gui.glade", &err)) {
        g_critical("Не вышло загрузить файл с UI : %s", err->message);
        g_error_free(err);
    }
    GtkWidget* window = GTK_WIDGET(gtk_builder_get_object(ui_builder, "form1"));
    gtk_builder_connect_signals(ui_builder, NULL);
    gtk_widget_show_all(window);

    FILE* file_config = fopen("config.conf", "r");
    int cb_vertices_index = 0;
    int cb_line_index = 0;
    int cb_proection_type_index = 0;
    int vertices_size = 0;
    int lines_size = 0;
    int ent_line_rgb_r = 0, ent_line_rgb_g = 0, ent_line_rgb_b = 0;
    int ent_lines_size = 0;
    int ent_vertices_rgb_r = 0, ent_vertices_rgb_g = 0, ent_vertices_rgb_b = 0;
    int etr_vertices_size = 0;
    int ent_background_rgb_r = 0, ent_background_rgb_g = 0, ent_background_rgb_b = 0;
    int ent_k = 0;
    fscanf(file_config, "%d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d",
            &cb_vertices_index, &cb_line_index, &cb_proection_type_index,
            &vertices_size, &lines_size,
            &ent_line_rgb_r, &ent_line_rgb_g, &ent_line_rgb_b,
            &ent_lines_size,
            &ent_vertices_rgb_r, &ent_vertices_rgb_g, &ent_vertices_rgb_b,
            &etr_vertices_size,
            &ent_background_rgb_r, &ent_background_rgb_g, &ent_background_rgb_b,
            &ent_k);
    fclose(file_config);
    char tmp[20];
    snprintf(tmp, sizeof(tmp), "%d %d %d", ent_line_rgb_r, ent_line_rgb_g, ent_line_rgb_b);
    set_text_to_widget("ent_line_rgb", &ui_builder, tmp);
    snprintf(tmp, sizeof(tmp), "%d %d %d", ent_vertices_rgb_r, ent_vertices_rgb_g, ent_vertices_rgb_b);
    set_text_to_widget("ent_vertices_rgb", &ui_builder, tmp);
    snprintf(tmp, sizeof(tmp), "%d %d %d", ent_background_rgb_r, ent_background_rgb_g, ent_vertices_rgb_b);
    set_text_to_widget("ent_background_rgb", &ui_builder, tmp);
    snprintf(tmp, sizeof(tmp), "%d", ent_lines_size);
    set_text_to_widget("ent_lines_size", &ui_builder, tmp);
    snprintf(tmp, sizeof(tmp), "%d", etr_vertices_size);
    set_text_to_widget("etr_vertices_size", &ui_builder, tmp);
    set_index_to_cb("cb_proection_type", &ui_builder, cb_proection_type_index);
    set_index_to_cb("cb_lines", &ui_builder, cb_line_index);
    set_index_to_cb("cb_vertices", &ui_builder, cb_vertices_index);
    snprintf(tmp, sizeof(tmp), "%d", ent_k);
    set_text_to_widget("ent_k", &ui_builder, tmp);
    gtk_main();
    return 0;
}
