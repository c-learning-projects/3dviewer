#include "vectors.h"

vector vector_create(float a, float b, float c) {
    vector new;
    new.x = a;
    new.y = b;
    new.z = c;
    new.w = 1;
    return new;
}

vector substruct(vector v1, vector v2) {
    vector new;
    new.x = v1.x - v2.x;
    new.y = v1.y - v2.y;
    new.z = v1.z - v2.z;
    new.w = v1.w - v2.w;
    return new;
}

vector crossProduct(vector a, vector b) {
    vector new;
    new.x = a.y * b.z - a.z * b.y;
    new.y = a.z * b.x - a.x * b.z;
    new.z = a.x * b.y - a.y * b.x;
    return new;
}

float  getLength(vector a) {
    return sqrt(a.x * a.x + a.y * a.y + a.z * a.z);
}

vector normalize(vector a) {
    float length = getLength(a);
    a.x /= length;
    a.y /= length;
    a.z /= length;
    return a;
}
