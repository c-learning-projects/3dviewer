#include "sub.h"
int get_widget_width(char* name, GtkBuilder **ui_builder) {
    GtkWidget* ent = GTK_WIDGET(gtk_builder_get_object(*ui_builder, name));
    return gtk_widget_get_allocated_width(ent);
}

int get_widget_height(char* name, GtkBuilder **ui_builder) {
    GtkWidget* ent = GTK_WIDGET(gtk_builder_get_object(*ui_builder, name));
    return gtk_widget_get_allocated_height(ent);
}

void draw_area_clean(cairo_t *cr, GtkBuilder **ui_builder) {
    set_rgb_from_entry(cr, "ent_background_rgb", ui_builder);
    cairo_paint(cr);
    cairo_stroke(cr);
}

double get_double_from_entry(char* name, GtkBuilder **ui_builder) {
    GtkWidget* ent = GTK_WIDGET(gtk_builder_get_object(*ui_builder, name));
    double res = 0;
    // setlocale(LC_ALL, "en_US.utf8");
    sscanf((char*)gtk_entry_get_text(GTK_ENTRY(ent)), "%lf", &res);
    return res;
}

void set_rgb_from_entry(cairo_t* cr, char* name, GtkBuilder **ui_builder) {
    GtkWidget* ent = GTK_WIDGET(gtk_builder_get_object(*ui_builder, name));
    int r = 0, g = 0, b = 0;
    sscanf((char*)gtk_entry_get_text(GTK_ENTRY(ent)), "%d %d %d", &r, &g, &b);
    cairo_set_source_rgb(cr, r, g, b);
}

int need_grani(cairo_t* cr, char* name1, char* name2, GtkBuilder **ui_builder) {
    GtkWidget* ent1 = GTK_WIDGET(gtk_builder_get_object(*ui_builder, name1));
    GtkWidget* ent2 = GTK_WIDGET(gtk_builder_get_object(*ui_builder, name2));
    return strcmp((char*)gtk_entry_get_text(GTK_ENTRY(ent1)), (char*)gtk_entry_get_text(GTK_ENTRY(ent2)));
}

void set_label_text(char* l_name, char* text, GtkBuilder **ui_builder) {
    GtkWidget* label = GTK_WIDGET(gtk_builder_get_object(*ui_builder, l_name));
    gtk_label_set_text(GTK_LABEL(label), text);
}

void set_label_num(char* l_name, int num, GtkBuilder **ui_builder) {
    char text[255] = {'\0'};
    snprintf(text, sizeof(text), "%d", num);
    GtkWidget* label = GTK_WIDGET(gtk_builder_get_object(*ui_builder, l_name));
    gtk_label_set_text(GTK_LABEL(label), text);
}

int get_int_from_label(char* l_name, GtkBuilder **ui_builder) {
    int res = 0;
    GtkWidget* label = GTK_WIDGET(gtk_builder_get_object(*ui_builder, l_name));
    const char* text = gtk_label_get_text(GTK_LABEL(label));
    sscanf(text, "%d", &res);
    return res;
}

const char* get_text_from_widget(char* name, GtkBuilder **ui_builder) {
    GtkEntry* ent = GTK_ENTRY(gtk_builder_get_object(*ui_builder, name));
    return gtk_entry_get_text(ent);
}

void set_text_to_widget(char* name, GtkBuilder **ui_builder, char* text) {
    GtkEntry* ent = GTK_ENTRY(gtk_builder_get_object(*ui_builder, name));
    gtk_entry_set_text(ent, text);
}

void set_index_to_cb(char* name, GtkBuilder **ui_builder, int index) {
    GtkWidget* cb = GTK_WIDGET(gtk_builder_get_object(*ui_builder, name));
    gtk_combo_box_set_active(GTK_COMBO_BOX(cb), index);
}
