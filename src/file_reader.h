#ifndef SRC_FILE_READER_H_
#define SRC_FILE_READER_H_
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>

typedef struct {
  float x;
  float y;
  float z;
} coord;

typedef struct {
  coord* point;
} polygons;

void Read_file_obj(char* name_file, coord** Mymass, polygons** models,
                    int* vertices, int* triangles, char* name);
#endif  // SRC_FILE_READER_H_
