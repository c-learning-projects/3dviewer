#include "calc.h"

matrix_t turn_y(float a) {
    a = M_PIf / 180.0 * a;
    matrix_t A = s21_create_matrix(4, 4);
    A.matrix[0][0] = cos(a); A.matrix[0][1] = 0; A.matrix[0][2] = sin(a); A.matrix[0][3] = 0;
    A.matrix[1][0] = 0; A.matrix[1][1] = 1; A.matrix[1][2] = 0; A.matrix[1][3] = 0;
    A.matrix[2][0] = -sin(a); A.matrix[2][1] = 0; A.matrix[2][2] = cos(a); A.matrix[2][3] = 0;
    A.matrix[3][0] = 0; A.matrix[3][1] = 0; A.matrix[3][2] = 0; A.matrix[3][3] = 1;
    return A;
}

matrix_t turn_x(float a) {
    matrix_t A = s21_create_matrix(4, 4);
    a = M_PIf / 180.0 * a;
    A.matrix[0][0] = 1; A.matrix[0][1] = 0; A.matrix[0][2] = 0; A.matrix[0][3] = 0;
    A.matrix[1][0] = 0; A.matrix[1][1] = cos(a); A.matrix[1][2] = -sin(a); A.matrix[1][3] = 0;
    A.matrix[2][0] = 0; A.matrix[2][1] = sin(a); A.matrix[2][2] = cos(a); A.matrix[2][3] = 0;
    A.matrix[3][0] = 0; A.matrix[3][1] = 0; A.matrix[3][2] = 0; A.matrix[3][3] = 1;
    return A;
}

matrix_t turn_z(float a) {
    matrix_t A = s21_create_matrix(4, 4);
    a = M_PIf / 180.0 * a;
    A.matrix[0][0] = cos(a); A.matrix[0][1] = -sin(a); A.matrix[0][2] = 0; A.matrix[0][3] = 0;
    A.matrix[1][0] = sin(a); A.matrix[1][1] = cos(a); A.matrix[1][2] = 0; A.matrix[1][3] = 0;
    A.matrix[2][0] = 0; A.matrix[2][1] = 0; A.matrix[2][2] = 1; A.matrix[2][3] = 0;
    A.matrix[3][0] = 0; A.matrix[3][1] = 0; A.matrix[3][2] = 0; A.matrix[3][3] = 1;
    return A;
}

matrix_t scale_f(float scale_step) {
    matrix_t A = s21_create_matrix(4, 4);
    float tx = 0, ty = 0, tz = 0;
    A.matrix[0][0] = scale_step; A.matrix[0][1] = 0; A.matrix[0][2] = 0; A.matrix[0][3] = 0;
    A.matrix[1][0] = 0; A.matrix[1][1] = scale_step; A.matrix[1][2] = 0; A.matrix[1][3] = 0;
    A.matrix[2][0] = 0; A.matrix[2][1] = 0; A.matrix[2][2] = scale_step; A.matrix[2][3] = 0;
    A.matrix[3][0] = 0; A.matrix[3][1] = 0; A.matrix[3][2] = 0; A.matrix[3][3] = 1;
    return A;
}

matrix_t move_f(float tx, float ty, float tz) {
    matrix_t A = s21_create_matrix(4, 4);
    A.matrix[0][0] = 1; A.matrix[0][1] = 0; A.matrix[0][2] = 0; A.matrix[0][3] = tx;
    A.matrix[1][0] = 0; A.matrix[1][1] = 1; A.matrix[1][2] = 0; A.matrix[1][3] = ty;
    A.matrix[2][0] = 0; A.matrix[2][1] = 0; A.matrix[2][2] = 1; A.matrix[2][3] = tz;
    A.matrix[3][0] = 0; A.matrix[3][1] = 0; A.matrix[3][2] = 0; A.matrix[3][3] = 1;
    return A;
}

matrix_t create_moves(float tx, float ty, float tz, float turn_xf, float turn_yf,
                        float turn_zf, float scale_step) {
    matrix_t A = move_f(tx, ty, tz);
    matrix_t C;
    if (turn_xf != 0.0) {
        matrix_t B = turn_x(turn_xf);
        C = s21_mult_matrix(&A, &B);
        s21_remove_matrix(&B); s21_remove_matrix(&A);
    }
    if (A.rows == 0) A = C;
    if (turn_yf != 0.0) {
        matrix_t B = turn_y(turn_yf);
        C = s21_mult_matrix(&A, &B);
        s21_remove_matrix(&B); s21_remove_matrix(&A);
    }
    if (A.rows == 0) A = C;
    if (turn_zf != 0.0) {
        matrix_t B = turn_z(turn_zf);
        C = s21_mult_matrix(&A, &B);
        s21_remove_matrix(&B); s21_remove_matrix(&A);
    }
    if (A.rows == 0) A = C;
    if (scale_step != 0.0) {
        matrix_t B = scale_f(scale_step);
        C = s21_mult_matrix(&A, &B);
        s21_remove_matrix(&B); s21_remove_matrix(&A);
    }
    if (A.rows == 0) A = C;
    return A;
}

// matrix_t getPerspectiveProjection(float fovy, float aspect, int n, int f) {
//     float radians = M_PIf / 180.0 * fovy;
//     float sx = (1 / tan(radians / 2.0)) / aspect;
//     float sy = (1 / tan(radians / 2.0));
//     float sz = (f + n) / (f - n);
//     float dz = (-2.0 * f * n) / (f - n);
//     matrix_t A = s21_create_matrix(4, 4);
//     A.matrix[0][0] = sx; A.matrix[0][1] = 0; A.matrix[0][2] = 0; A.matrix[0][3] = 0;
//     A.matrix[1][0] = 0; A.matrix[1][1] = sy; A.matrix[1][2] = 0; A.matrix[1][3] = 0;
//     A.matrix[2][0] = 0; A.matrix[2][1] = 0; A.matrix[2][2] = sz; A.matrix[2][3] = dz;
//     A.matrix[3][0] = 0; A.matrix[3][1] = 0; A.matrix[3][2] = -1; A.matrix[3][3] = 0;
//     return A;
//   }

// matrix_t getLookAt(vector eye, vector target, vector up) {
//     vector vz = normalize(substruct(eye, target));
//     vector vx = normalize(crossProduct(up, vz));
//     vector vy = normalize(crossProduct(vz, vx));
//     matrix_t A = move_f(-eye.x, -eye.y, -eye.z);
//     matrix_t B = s21_create_matrix(4, 4);
//     A.matrix[0][0] = vx.x; A.matrix[0][1] = vx.y; A.matrix[0][2] = vx.z; A.matrix[0][3] = 0;
//     A.matrix[1][0] = vy.x; A.matrix[1][1] = vy.y; A.matrix[1][2] = vz.z; A.matrix[1][3] = 0;
//     A.matrix[2][0] = vy.x; A.matrix[2][1] = vy.y; A.matrix[2][2] = vz.z; A.matrix[2][3] = 0;
//     A.matrix[3][0] = 0; A.matrix[3][1] = 0; A.matrix[3][2] = -0; A.matrix[3][3] = 1;
//     return s21_mult_matrix(&A, &B);
// }

// matrix_t create_perspective(matrix_t* A, vector eye, vector target, vector up,
//                              float fovy, float aspect, int n, int f) {
//     matrix_t B = getLookAt(eye, target, up);
//     matrix_t C = getPerspectiveProjection(fovy, aspect, n, f);
//     matrix_t res;
//     res = s21_mult_matrix(A, &B);
//     res = s21_mult_matrix(A, &C);
//     s21_remove_matrix(&B); s21_remove_matrix(&C); s21_remove_matrix(A);
//     return res;
// }

void xyz_res(matrix_t* A, float x, float y, float z, float *xr, float *yr, float *zr) {
    matrix_t B = s21_create_matrix(4, 1);
    B.matrix[0][0] = x; B.matrix[1][0] = y; B.matrix[2][0] = z; B.matrix[3][0] = 1;
    matrix_t C = s21_mult_matrix(A, &B);
    *xr = C.matrix[0][0];
    *yr = C.matrix[1][0];
    *zr = C.matrix[2][0];
    s21_remove_matrix(&B); s21_remove_matrix(&C);
}
