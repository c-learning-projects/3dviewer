#include <check.h>
#include "../calc.h"
#include "../file_reader.h"

int cmpFloats(float num1, float num2) {
    if (isnan(num1) && isnan(num2)) return 1;
    if (isinf(num1) && isinf(num2)) return 1;
    return (fabs(num1 - num2) < 0.0000001);
}

void clean(coord *Mymass, polygons *models, int triangles) {
    free(Mymass);
    for (int i = 0; i < triangles; i++) {
            free(models[i].point);
    }
    free(models);
}

START_TEST(create_moves_test) {
    matrix_t res = create_moves(1.2, 3.4, 5.6, 7.8, 9.10, 11.12, 13.14);
    matrix_t exp = s21_create_matrix(4, 4);
    exp.matrix[0][0] = 12.731024461;
    exp.matrix[0][1] = -2.502343141;
    exp.matrix[0][2] = 2.078197296;
    exp.matrix[0][3] = 1.200000048;
    exp.matrix[1][0] = 2.787540867;
    exp.matrix[1][1] = 12.719614977;
    exp.matrix[1][2] = -1.760857682;
    exp.matrix[1][3] = 3.400000095;
    exp.matrix[2][0] = -1.676377377;
    exp.matrix[2][1] = 2.146924001;
    exp.matrix[2][2] = 12.854574487;
    exp.matrix[2][3] = 5.599999905;
    exp.matrix[3][0] = 0;
    exp.matrix[3][1] = 0;
    exp.matrix[3][2] = 0;
    exp.matrix[3][3] = 1;
    ck_assert(s21_eq_matrix(&res, &exp));
    s21_remove_matrix(&res); s21_remove_matrix(&exp);
}
END_TEST

START_TEST(xyz_res_test) {
    matrix_t mtr = create_moves(1.2, 3.4, 5.6, 7.8, 9.10, 11.12, 13.14);
    float res1;
    float res2;
    float res3;

    float exp1 = 6.79147577;
    float exp2 = 209.81652832;
    float exp3 = 67.89415741;
    xyz_res(&mtr, 3.14, 15.9, 2.6, &res1, &res2, &res3);
    ck_assert(cmpFloats(exp1, res1) &&
              cmpFloats(exp2, res2) &&
              cmpFloats(exp3, res3));
    s21_remove_matrix(&mtr);
}
END_TEST

START_TEST(reader) {
    coord *Mymass;
    polygons *models;
    int vertices = 0, triangles = 0;
    char name[150];
    Read_file_obj("tests/cube.obj", &Mymass, &models, &vertices, &triangles, name);
    ck_assert(vertices == 8 && triangles == 13 && !strcmp(name, "''\n"));
    clean(Mymass, models, triangles);
}
END_TEST

Suite *new_suite_create(void) {
    Suite *suite = suite_create("main suite");
    TCase *cases = tcase_create("cases");
    tcase_add_test(cases, create_moves_test);
    tcase_add_test(cases, xyz_res_test);
    tcase_add_test(cases, reader);
    suite_add_tcase(suite, cases);
    return suite;
}

int main(void) {
    Suite *suite = new_suite_create();
    SRunner *suite_runner = srunner_create(suite);
    srunner_run_all(suite_runner, CK_NORMAL);
    int failed_count = srunner_ntests_failed(suite_runner);
    srunner_free(suite_runner);
    return failed_count;
}
