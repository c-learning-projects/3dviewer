#include "file_reader.h"

void Compare(float* Coords, int* indexs, int count_fase, int count_point, coord** Mymass, polygons** models) {
  int point = 1, p = 0;
  *Mymass = (coord*)calloc(count_point / 3 + 1, sizeof(coord));
  *models = (polygons*)calloc(count_fase / 4, sizeof(polygons));
  for (int i = 0; i < count_point; i += 3) {
    (*Mymass)[point].x = Coords[i + 0];
    (*Mymass)[point].y = Coords[i + 1];
    (*Mymass)[point].z = Coords[i + 2];
    point++;
  }
  p = 0;
  for (int f = 0; f < count_fase; f += 4) {
    (*models)[p].point = calloc(4, sizeof(coord));
    (*models)[p].point[0] = (*Mymass)[indexs[f]];
    (*models)[p].point[1] = (*Mymass)[indexs[f + 1]];
    (*models)[p].point[2] = (*Mymass)[indexs[f + 2]];
    (*models)[p].point[3] = (*Mymass)[indexs[f + 3]];
    p++;
  }
}

int size_of_file(FILE** name) {
  fseek(*name, 0L, SEEK_END);
  int File_Size = ftell(*name);
  rewind(*name);
  return File_Size;
}

int read_line(char* line, int* i) {
  int res = 0, n = strlen(line);
  while (*i < n - 1 && line[*i] != ' ') (*i)++;
  sscanf(line+*i, "%d", &res);
  char temp[10]; snprintf(temp, sizeof(temp), "%d", res);
  *i = (strstr(&line[*i], temp) - &line[*i]) + *i;
  return res;
}

void Read_file_obj(char* name_file, coord** Mymass, polygons** models,
                    int* vertices, int* triangles, char* name) {
  setlocale(LC_ALL, "en_US.utf8");
  FILE* My_File_obj = fopen(name_file, "r");
  if (My_File_obj != NULL) {
    char line[100];
    int coords_index = 0, Fase_index = 0;
    int countpoint = 0;
    int countfase = 0;
    int File_Size = size_of_file(&My_File_obj);
    float* Tmp_Coords = (float*)calloc(File_Size, sizeof(float));
    int* Tmp_faseArray = (int*)calloc(File_Size, sizeof(int));
    fgets(line, sizeof(line), My_File_obj);
    snprintf(name, sizeof(name), "%s", strchr(line, '\''));
    while (!feof(My_File_obj)) {
      fgets(line, sizeof(line), My_File_obj);
      if ((line[0] == 'v') && (line[1] == ' ')) {
        sscanf(&line[2], "%f %f %f",
                &Tmp_Coords[coords_index + 0],
                &Tmp_Coords[coords_index + 1],
                &Tmp_Coords[coords_index + 2]);
        coords_index += 3;
        countpoint++;
      }
      if ((line[0] == 'f') && (line[1] ==' ')) {
        int i = 1;
        Tmp_faseArray[Fase_index + 0] = read_line(line, &i);
        Tmp_faseArray[Fase_index + 1] = read_line(line, &i);
        Tmp_faseArray[Fase_index + 2] = read_line(line, &i);
        Tmp_faseArray[Fase_index + 3] = read_line(line, &i);
      Fase_index += 4;
      countfase++;
      }
    }
    *vertices = countpoint, *triangles = countfase;
    Compare(Tmp_Coords, Tmp_faseArray, Fase_index, coords_index, Mymass, models);
    free(Tmp_Coords); free(Tmp_faseArray);
  } else {
    printf("\nОшибка чтения из файла\n");
  }
}
