#ifndef SRC_SUB_H_
#define SRC_SUB_H_
#include <gtk/gtk.h>
#include <cairo.h>
#include <locale.h>

int get_widget_width(char* name, GtkBuilder **ui_builder);
int get_widget_height(char* name, GtkBuilder **ui_builder);
void draw_area_clean(cairo_t *cr, GtkBuilder **ui_builder);
double get_double_from_entry(char* name, GtkBuilder **ui_builder);
void set_label_text(char* l_name, char* text, GtkBuilder **ui_builder);
void set_label_num(char* l_name, int num, GtkBuilder **ui_builder);
int get_int_from_label(char* l_name, GtkBuilder **ui_builder);
void set_rgb_from_entry(cairo_t* cr, char* name, GtkBuilder **ui_builder);
const char* get_text_from_widget(char* name, GtkBuilder **ui_builder);
void set_text_to_widget(char* name, GtkBuilder **ui_builder, char* text);
void set_index_to_cb(char* name, GtkBuilder **ui_builder, int index);
int need_grani(cairo_t* cr, char* name1, char* name2, GtkBuilder **ui_builder);
#endif  // SRC_SUB_H_
