#ifndef SRC_CALC_H_
#define SRC_CALC_H_
#include "s21_matrix.h"
#include <math.h>
// #include "vectors.h"

#ifndef M_PIf
#define M_PIf 3.14159265358979323846f
#endif

matrix_t create_moves(float tx, float ty, float tz, float turn_xf,
                        float turn_yf, float turn_zf, float scale_step);
void xyz_res(matrix_t* A, float x, float y, float z, float *xr, float *yr, float *zr);
// matrix_t create_perspective(matrix_t* A, vector eye, vector target,
//                                vector up, float fovy, float aspect, int n, int f);
#endif  // SRC_CALC_H_
